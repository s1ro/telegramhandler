<?php

namespace TelegramHandler\Handler;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use TelegramHandler\Formatter\TelegramFormatter;

class TelegramHandler extends AbstractProcessingHandler
{
    private const BASE_URI = 'https://api.telegram.org';

    /**
     * @var string
     */
    private $telegramToken;

    /**
     * @var string
     */
    private $telegramChatId;

    /**
     * @var bool
     */
    private $useCurl;

    /**
     * @var int
     */
    private $timeout;

    /**
     * @var bool
     */
    private $verifyPeer;

    /**
     * @var bool
     */
    private $ignore404;

    /**
     * @var bool
     */
    private $ignore403;

    /**
     * @var bool
     */
    private $ignoreMethodNotAllowed;

    public function __construct(
        string $telegramToken,
        string $telegramChatId,
        $level = Logger::ERROR,
        bool $bubble = true,
        int $timeout = 10,
        $useCurl = true,
        $verifyPeer = true,
        bool $ignore404 = true,
        bool $ignore403 = true,
        bool $ignoreMethodNotAllowed = true
    )
    {
        $this->telegramToken = $telegramToken;
        $this->telegramChatId = $telegramChatId;
        $this->timeout = $timeout;
        $this->useCurl = $useCurl;
        $this->verifyPeer = $verifyPeer;
        $this->ignore404 = $ignore404;
        $this->ignore403 = $ignore403;
        $this->ignoreMethodNotAllowed = $ignoreMethodNotAllowed;
        $this->formatter = new TelegramFormatter();
        parent::__construct($level, $bubble);
    }

    /**
     * @param string $message
     * @return bool
     */
    public function send(string $message): bool
    {
        $url = self::BASE_URI . '/bot' . $this->telegramToken . '/sendMessage';
        $data = [
            'chat_id' => $this->telegramChatId,
            'text' => $message,
            'disable_web_page_preview' => true
        ];

        if (preg_match('/<[^<]+>/', $data['text'])) {
            $data['parse_mode'] = 'HTML';
        }

        if ($this->useCurl && extension_loaded('curl')) {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verifyPeer);
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);

            $result = curl_exec($ch);
            if (!$result) {
                throw new \RuntimeException('Request to Telegram API failed: ' . curl_error($ch));
            }
        } else {
            $opts = [
                'http' => [
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($data),
                    'timeout' => $this->timeout
                ],
                'ssl' => [
                    'verify_peer' => $this->verifyPeer,
                    'verify_peer_name' => $this->verifyPeer
                ]
            ];

            $result = @file_get_contents($url, false, stream_context_create($opts));
            if (!$result) {
                $error = error_get_last();
                if (isset($error['message'])) {
                    throw new \RuntimeException('Request to api failed: ' . $error['message']);
                }

                throw new \RuntimeException('Request to api failed!');
            }
        }

        $result = json_decode($result, true);
        if (isset($result['ok']) && $result['ok']) {
            return true;
        }

        if (isset($result['description'])) {
            throw new \RuntimeException('Telegram API error: ' . $result['description']);
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    protected function write(array $record): void
    {
        $message = $record['formatted'] ?? $record['message'];

        if ($this->ignore404 && strpos($message, 'NotFoundHttpException')) {
            return;
        }

        if ($this->ignore403 && strpos($message,'AccessDeniedHttpException')) {
            return;
        }

        if ($this->ignoreMethodNotAllowed && strpos($message, 'MethodNotAllowedHttpException')) {
            return;
        }

        if (mb_strlen($message, 'UTF-8') > 4096) {
            $message = strip_tags($message);
        }

        do {
            $message_part = mb_substr($message, 0, 4096);
            $this->send($message_part);
            $message = mb_substr($message, 4096);
        } while ($message !== '');
    }

    /**
     * {@inheritDoc}
     */
    public function handleBatch(array $records): void
    {
        $messages = [];
        foreach ($records as $record) {
            if ($record['level'] < $this->level) {
                continue;
            }

            $messages = $this->processRecord($record);
        }

        if (!empty($messages)) {
            $this->write(['formatted' => $this->getFormatter()->formatBatch($messages)]);
        }
    }
}
