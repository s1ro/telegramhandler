<?php

namespace TelegramHandler\Formatter;

use Monolog\Formatter\NormalizerFormatter;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Dumper\YamlDumper;

class TelegramFormatter extends NormalizerFormatter
{
    private const STACK_TRACE_KEY = 'Stack trace';

    private static $logLevelEmojiMapping = [
        Logger::DEBUG => '🔵',
        Logger::INFO => '🟢',
        Logger::NOTICE => '🟡',
        Logger::WARNING => '🟠',
        Logger::ERROR => '🔴',
        Logger::CRITICAL => '🔴',
        Logger::ALERT => '🔴',
        Logger::EMERGENCY => '⚫'
    ];

    /**
     * @var bool
     */
    private $showContextStackTrace;

    public function __construct(
        bool $showContextStackTrace = false,
        ?string $dateFormat = null
    )
    {
        $this->showContextStackTrace = $showContextStackTrace;
        parent::__construct($dateFormat);
    }

    /**
     * @inheritDoc
     */
    public function format(array $record): string
    {
        $levelEmoji = self::$logLevelEmojiMapping[$record['level']];
        $output = "<b>{$levelEmoji}{$record['level_name']}{$levelEmoji}</b>\n";
        $output .= "<b>Message:</b> {$record['message']}\n";
        $output .= "<b>Time:</b> {$record['datetime']->format($this->dateFormat)}\n";
        $output .= "<b>Channel:</b> {$record['channel']}\n";

        if ($record['context']) {
            $output .= "\n[context]";
            $output .= $this->formatContextAndExtra($record['context']);
        }

        if ($record['extra']) {
            $output .= "\n[extra]";
            $output .= $this->formatContextAndExtra($record['extra']);
        }

        return $output;
    }

    /**
     * @inheritDoc
     */
    public function formatBatch(array $records)
    {
        $message = '';
        foreach ($records as $record) {
            $message .= $this->format($record);
        }
        return $message;
    }

    /**
     * @param array $data
     * @return string
     */
    public function formatContextAndExtra(array $data): string
    {
        $result = '';
        foreach ($data as $key => $value) {
            if (!$this->showContextStackTrace && $key === self::STACK_TRACE_KEY) {
                continue;
            }
            $result .= "\n{$key}: {$value}";
        }
        return $result . "\n";
    }

}